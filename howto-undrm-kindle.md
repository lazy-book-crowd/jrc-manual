# Remove DRM from Amazon Kindle’s ebooks using Linux

September 28, 2011 at 11:51pm on [fb](https://www.facebook.com/notes/unix/remove-drm-from-amazon-kindles-ebooks-using-linux/293892687293406/)

When you buy an ebook using your Kindle, there’s a 99% chance it’s a DRM file. That means you won’t be able to read it anywhere else but on your Kindle. And we won’t accept that. To be able to convert that file and remove its DRM protection, you need a set of Python scripts called [MobiDeDRM]

Now open your Kindle and type *411* on the keypad. This will bring up a window containing information. Write down the 16-character alphanumeric Serial.

Through your computer, buy a book from [Amazon.com](http://Amazon.com/) and save it to your hard drive. The filename should be something like *Book-Title.azw*.

Be sure you have Python installed in Linux. In Ubuntu you can easily install it with

```sh
    sudo apt-get install python.
```

Extract the files from the [MobiDeDRM] file and run one of them like this:

```sh
    python kindlepid.py XXXXXXXXXXX
```

where XXXXXXXXXXXX is your 16-character Serial. The Terminal window will return something like

_Mobipocked PID for Kindle serial# XXXXXXXXXXXXXXXX is Z1QFCDQ*74_

That “_Z1QFCDQ*74_” string is what you need.
Now all you have to do to remove the DRM from the .AZW file is:

```sh
    python mobidedrm.py Book-Title.azw Book-Title.mobi Z1QFCDQ*74
```

In a minute or so you should have a DRM-free MobiPocket book you can later convert in any other format you like. If that doesn’t work, try

```sh
    python mobidedrm2.py Book-Title.azw Book-Title.mobi Z1QFCDQ*74
```

## Convert eBooks in Linux

Say you just bought an Amazon Kindle or a Barnes and Noble Nook. You want to convert your eBook collection to .EPUB or .MOBI format. For this, install [Calibre]. The application not only provides you with a graphical way to manage your eBook collection, but also comes with a set of useful command-line tools. One of these is ebook-convert.

This tool can help you convert between tens of standard formats like EPUB, FB2, LIT, LRF, MOBI, OEB, PDB, PDF, PML, RB, RTF, TCR, TXT, HTML and more. Even CBR and CBZ (comic book formats) are supported.

The syntax is simple:

```sh
    ebook-convert input_format output_format
```

For example, if you want to convert a comic book archive to something readable on an e-ink screen, use

```
    ebook-convert filename.cbz filename.epub
```

or

```
    ebook-convert filename.cbz filename.pdf
```

Not only will *ebook-convert* convert the file, but it will also trim the white spaces around the page so they will better fit the small screen of an electronic eBook reader.

Courtesy: [Tips4Linux.com] - Tips and Tricks for Linux Users, [Marc Telesha]

[MobiDeDRM]: http://nyquil.org/uploads/MobiDeDRM.zip
[Calibre]: http://calibre-ebook.com/
[Tips4Linux.com]: http://Tips4Linux.com/
[Marc Telesha]: https://plus.google.com/u/0/102611240660807554839